Configuracion inicial

Git global setup

git config --global user.name "juan alejandro leon"
git config --global user.email "juanalejandroleon15@gmail.com"

Create a new repository

git clone https://gitlab.com/juanalejandroleon15/e-boutique.git
cd e-boutique
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main


Push an existing folder

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/juanalejandroleon15/e-boutique.git
git add .
git commit -m "Initial commit"
git push -u origin main


Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/juanalejandroleon15/e-boutique.git
git push -u origin --all
git push -u origin --tags


